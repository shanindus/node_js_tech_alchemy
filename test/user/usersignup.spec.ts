import { expect } from 'chai';
import request from 'supertest';

import app from '../../src/app';
// import { connect, close } from '../../src/config/database/mongo';

describe('POST /api/v1/user/signup', () => {
  //   before((done) => {
  //     connect();
  //   });
  //   after((done) => {
  //     close();
  //   });

  it('Creating a user', (done) => {
    request(app)
      .post('/api/v1/user/signup')
      .set('x-api-key', '8qz5gaEXqqH98&Q5USDNHm$uHUq*xADs&Z*9Hxfq$6vE!&$p#9')
      .send({
        firstName: 'Shankha',
        lastName: 'Chatterjee',
        email: 'test2@yopmail.com',
        password: '123456',
      })
      .then((res) => {
        expect(res.status).to.equal(200);
        done();
      })
      .catch((err) => done(err));
  });
});
