import { expect } from 'chai';
import request from 'supertest';

import app from '../../src/app';

describe('POST /api/v1/user/login', () => {
  it('User Login', (done) => {
    request(app)
      .post('/api/v1/user/login')
      .set('x-api-key', '8qz5gaEXqqH98&Q5USDNHm$uHUq*xADs&Z*9Hxfq$6vE!&$p#9')
      .send({
        email: 'shanman@yopmail.com',
        password: '123456',
      })
      .then((res) => {
        expect(res.status).to.equal(200);
        done();
      })
      .catch((err) => done(err));
  });
});
