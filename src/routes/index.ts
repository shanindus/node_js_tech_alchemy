import { Router } from 'express';
import { newsRouter } from './news';
import { userRouter } from './user';
import { weatherRouter } from './weather';
import { candidateRouter } from './candidate';

const v1Router = Router();

// All routes go here
v1Router.use('/user', userRouter);
v1Router.use('/news', newsRouter);
v1Router.use('/weather', weatherRouter);
v1Router.use('/candidate', candidateRouter);

export { v1Router };
