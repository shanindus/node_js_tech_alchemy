import { Router } from 'express';
import { WeatherController } from '@controllers/weather';
import { validateApiKey } from '@middleware/index';

const weatherRouter = Router();
const weatherCtrl = new WeatherController();

weatherRouter.use(validateApiKey);

weatherRouter.get('/', weatherCtrl.fetchWeatherForecast);

export { weatherRouter };
