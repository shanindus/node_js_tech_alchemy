import { Router } from 'express';
import { validateApiKey, validateUserAccessToken } from '@middleware/index';
import { CandidateController } from '@controllers/candidate';

const candidateRouter = Router();
const candidateCtrl = new CandidateController();

candidateRouter.use(validateApiKey);

candidateRouter.post('/add', validateUserAccessToken, candidateCtrl.addCandidate);

candidateRouter.get('/', validateUserAccessToken, candidateCtrl.fetchCandidate);

candidateRouter.post('/vote', validateUserAccessToken, candidateCtrl.castVote);

export { candidateRouter };
