import { Router } from 'express';
import { UserController } from '@controllers/user';
import {
  validateApiKey,
  validateUserAccessToken,
  validateUserRefreshToken,
} from '@middleware/index';

import { userSignup, userLogin } from '@validations/user';

const userRouter = Router();
const userCtrl = new UserController();

userRouter.use(validateApiKey);

userRouter.post('/signup', userSignup, userCtrl.createUser);

userRouter.post('/login', userLogin, userCtrl.useLogin);

userRouter.get('/me', validateUserAccessToken, userCtrl.getCurrentUserDetails);

userRouter.get('/token', validateUserRefreshToken, userCtrl.genrateNewToken);

export { userRouter };
