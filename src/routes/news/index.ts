import { Router } from 'express';
import { NewsController } from '@controllers/news';
import { validateApiKey, validateUserAccessToken } from '@middleware/index';

const newsRouter = Router();
const newsCtrl = new NewsController();

import { newsSearch } from '@validations/news';

newsRouter.use(validateApiKey);

newsRouter.get('/', validateUserAccessToken, newsCtrl.fetchNewsHeadlines);

newsRouter.get(
  '/search/:searchparam',
  validateUserAccessToken,
  newsSearch,
  newsCtrl.fetchNewsSearchHeadlines,
);

export { newsRouter };
