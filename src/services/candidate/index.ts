import voteCandidateModel from '@modules/votecandidate/schema';
import { IAddCandidateRequest, ICandidate } from '@modules/votecandidate/model';

export class CandidateService {
  /**
   * Save new user into db
   * @param candidateDetails
   */
  public async saveCandidate(candidateDetails: IAddCandidateRequest): Promise<ICandidate> {
    const newCandidate: ICandidate = new voteCandidateModel({
      name: {
        first: candidateDetails.firstName,
        last: candidateDetails.lastName,
      },
      email: candidateDetails.email,
      image_url: candidateDetails.image_url,
      title: candidateDetails.title,
    });

    await newCandidate.save();
    return newCandidate;
  }

  public async checkUniqueEmail(email: string): Promise<boolean> {
    const condition = {
      email: {
        // eslint-disable-next-line no-useless-escape
        $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
      },
    };

    const emailCount = await voteCandidateModel.countDocuments(condition);

    return emailCount > 0 ? false : true;
  }

  public async fetchCandidate() {
    return await voteCandidateModel.find({});
  }
}
