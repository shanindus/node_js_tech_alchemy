import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import userModel from '@modules/users/schema';
import { ICreateUserRequest, IUser, IUserTokens, IUserRequestObject } from '@modules/users/model';
import { envs } from '@config/env';

export class UserService {
  /**
   * email existence check within DB
   * @param email
   */
  public async checkUniqueEmail(email: string): Promise<boolean> {
    const condition = {
      email: {
        // eslint-disable-next-line no-useless-escape
        $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
      },
    };

    const emailCount = await userModel.countDocuments(condition);

    return emailCount > 0 ? false : true;
  }

  /**
   * Save new user into db
   * @param userDetails
   */
  public async saveUser(userDetails: ICreateUserRequest): Promise<IUser> {
    const newUser: IUser = new userModel({
      name: {
        first: userDetails.firstName,
        last: userDetails.lastName,
      },
      email: userDetails.email,
      password: userDetails.password,
    });

    await newUser.save();
    return newUser;
  }

  public async getUserDetailsByEmail(email: string): Promise<IUser | null> {
    const condition = {
      email: {
        // eslint-disable-next-line no-useless-escape
        $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
      },
    };
    const selection = {
      __v: 0,
    };
    const userDetails = await userModel.findOne(condition, selection);
    return userDetails;
  }
  /**
   * This function is used for creating user token
   * @param userDetails
   */
  public genrateUserTokens(userDetails: IUser | IUserRequestObject): IUserTokens {
    const accessTokenExpiry = envs.jwt.accessToken.expiry;
    const refreshTokenExpiry = envs.jwt.refreshToken.expiry;
    const accessToken = jwt.sign(
      { id: userDetails._id, name: userDetails.name, email: userDetails.email },
      envs.jwt.accessToken.secret,
      { expiresIn: accessTokenExpiry },
    );
    const refreshToken = jwt.sign(
      { id: userDetails._id, name: userDetails.name, email: userDetails.email },
      envs.jwt.refreshToken.secret,
      { expiresIn: refreshTokenExpiry },
    );
    return {
      accessToken: accessToken,
      accessTokenExpiry: accessTokenExpiry,
      refreshToken: refreshToken,
      refreshTokenExpiry: refreshTokenExpiry,
      name: userDetails.name,
      email: userDetails.email,
    };
  }
  /**
   * This function is used for validating user token
   * @param token
   * @param tokenSecret
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public verifyUserToken(token: string, tokenSecret: string): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.verify(token, tokenSecret, (err, decodeData) => {
        if (err) {
          reject(err);
        } else {
          resolve(decodeData);
        }
      });
    });
  }
  /**
   * This function is used for getting the user details by id
   * @param id
   * @param callback
   */
  public async getUserDetailsById(id: string): Promise<IUser | null> {
    const condition = {
      _id: mongoose.Types.ObjectId(id),
    };
    const selection = {
      _id: 1,
      name: 1,
      email: 1,
    };
    const userDetails = await userModel.findOne(condition, selection);
    return userDetails;
  }
}
