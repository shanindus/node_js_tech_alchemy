import { INewsHeadlines, INewsArticles } from '@modules/news/model';
import axios, { AxiosResponse } from 'axios';
import { envs } from '@config/env';

export class NewsService {
  public async newsHeadlines(): Promise<INewsHeadlines | null> {
    const newsData: AxiosResponse = await axios.get(
      `https://newsapi.org/v2/top-headlines?country=us&apiKey=${envs.newsApiToken}`,
    );

    if (newsData.data) {
      return this.parseNewsApiData(newsData);
    }

    return null;
  }

  public async newsSearchHeadlines(searchParams: string): Promise<INewsHeadlines | null> {
    const newsData: AxiosResponse = await axios.get(
      `https://newsapi.org/v2/everything?q=${searchParams}&apiKey=${envs.newsApiToken}`,
    );
    if (newsData.data) {
      if (newsData.data.totalResults === 0) {
        return this.newsHeadlines();
      } else {
        return this.parseNewsApiData(newsData);
      }
    }

    return null;
  }

  private parseNewsApiData(newsData: AxiosResponse): INewsHeadlines {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const articleData: [INewsArticles] = newsData.data.articles.map((article: any) => {
      return {
        headline: article.title,
        link: article.url,
      };
    });

    return {
      count: newsData.data.totalResults,
      data: articleData,
    };
  }
}
