import { IWeatherForecast, IWeatherList } from '@modules/weather/model';
import axios, { AxiosResponse } from 'axios';
import { envs } from '@config/env';

export class WeatherService {
  public async weatherForecast(): Promise<IWeatherForecast | null> {
    const weatherData: AxiosResponse = await axios.get(
      `https://api.openweathermap.org/data/2.5/forecast?q=Bangalore&appid=${envs.weatherApiToken}`,
    );

    if (weatherData.data) {
      return this.parseWeatherApiData(weatherData);
    }

    return null;
  }

  private parseWeatherApiData(weatherData: AxiosResponse): IWeatherForecast {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const listData: [IWeatherList] = weatherData.data.list.map((list: any) => {
      return {
        date: new Date(list.dt_txt).toDateString(),
        main: list.weather[0].main,
        temp: list.main.temp,
      };
    });

    return {
      count: weatherData.data.cnt,
      location: weatherData.data.city.name,
      data: listData,
    };
  }
}
