import { Request, Response } from 'express';
import { NewsService } from '@services/news';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';

export class NewsController {
  private newsService: NewsService;
  constructor() {
    this.newsService = new NewsService();
  }

  /*
   * This function is used for fetch Top Headline news from newsapi
   */
  public fetchNewsHeadlines = controller(
    async (req: Request, res: Response): Promise<void> => {
      const newsHeadlineData = await this.newsService.newsHeadlines();
      if (!newsHeadlineData) throw StatusError.badRequest('Invalid Request');
      res.send(newsHeadlineData);
    },
  );

  /*
   * This function is used for fetch Top Headline news based on the search parameters that user provide from newsapi
   */
  public fetchNewsSearchHeadlines = controller(
    async (req: Request, res: Response): Promise<void> => {
      const searchParam: string = req.params.searchparam;
      const newsHeadlineSearch = await this.newsService.newsSearchHeadlines(searchParam);
      if (!newsHeadlineSearch) throw StatusError.badRequest('Invalid Request');
      res.send(newsHeadlineSearch);
    },
  );
}
