import { Request, Response } from 'express';
import { WeatherService } from '@services/weather';
// import { ICreateUserRequest, ILoginUserRequest } from '@modules/users/model';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';

export class WeatherController {
  private weatherService: WeatherService;
  constructor() {
    this.weatherService = new WeatherService();
  }

  /*
   * This function is used for fetch weather forecast for 5 days per 3 hour from openweathermap API
   */
  public fetchWeatherForecast = controller(
    async (req: Request, res: Response): Promise<void> => {
      const weatherForecastData = await this.weatherService.weatherForecast();
      if (!weatherForecastData) throw StatusError.badRequest('Invalid Request');
      res.send(weatherForecastData);
    },
  );
}
