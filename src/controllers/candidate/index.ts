import { Request, Response } from 'express';
import { ILoginUserRequest } from '@modules/users/model';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { IAddCandidateRequest } from '@modules/votecandidate/model';
import { CandidateService } from '@services/candidate';

export class CandidateController {
  private candidateService: CandidateService;

  constructor() {
    this.candidateService = new CandidateService();
  }

  /**
   * This function is used for user Login
   * @param req
   * @param res
   */
  public fetchCandidate = controller(
    async (req: Request, res: Response): Promise<void> => {
      res.send(await this.candidateService.fetchCandidate());
    },
  );

  public addCandidate = controller(
    async (req: Request, res: Response): Promise<void> => {
      const reqBody: IAddCandidateRequest = req.body;

      // get user details by email
      const userDetails = await this.candidateService.checkUniqueEmail(reqBody.email);
      if (!userDetails) {
        throw StatusError.badRequest('Please enter unique Email id');
      }

      res.send(await this.candidateService.saveCandidate(reqBody));
    },
  );

  public castVote = controller(
    async (req: Request, res: Response): Promise<void> => {
      const reqBody: ILoginUserRequest = req.body;

      // get user details by email
      //   const voteDetails = await this.voteService.vote(reqBody.email);
      //   if (!voteDetails) {
      //     throw StatusError.badRequest('invaildEmailOrPassword');
      //   }

      //   res.send(this.userService.genrateUserTokens(userDetails));
    },
  );
}
