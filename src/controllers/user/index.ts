import { Request, Response } from 'express';
import { UserService } from '@services/users';
import { ICreateUserRequest, ILoginUserRequest } from '@modules/users/model';
import bcrypt from 'bcrypt';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { envs } from '@config/env';

export class UserController {
  private userService: UserService;
  private passwordSaltRound: number;

  constructor() {
    this.userService = new UserService();
    this.passwordSaltRound = envs.passwordSalt;
  }

  /**
   * Add new user
   */
  public createUser = controller(
    async (req: Request, res: Response): Promise<void> => {
      // existing user verification
      const reqBody: ICreateUserRequest = req.body;

      const isEmailUnique = await this.userService.checkUniqueEmail(reqBody.email);
      if (!isEmailUnique) {
        throw StatusError.badRequest('duplicateEmail');
      }
      reqBody.password = await bcrypt.hash(reqBody.password, this.passwordSaltRound);
      await this.userService.saveUser(reqBody);
      res.sendStatus(200);
    },
  );
  /**
   * This function is used for user Login
   * @param req
   * @param res
   */
  public useLogin = controller(
    async (req: Request, res: Response): Promise<void> => {
      const reqBody: ILoginUserRequest = req.body;

      // get user details by email
      const userDetails = await this.userService.getUserDetailsByEmail(reqBody.email);
      if (!userDetails) {
        throw StatusError.badRequest('invaildEmailOrPassword');
      }

      // password compare process
      const isSame = await bcrypt.compare(reqBody.password, userDetails.password);
      if (!isSame) {
        throw StatusError.badRequest('invaildEmailOrPassword');
      }

      res.send(this.userService.genrateUserTokens(userDetails));
    },
  );

  /**
   * This function is used for fetching current user details
   * @param req
   * @param res
   */
  public getCurrentUserDetails = controller(
    async (req: Request, res: Response): Promise<void> => {
      res.send(req.userDetails);
    },
  );

  /**
   * This function is used for genrating new refresh token
   * @param req
   * @param res
   */
  public genrateNewToken = controller(
    async (req: Request, res: Response): Promise<void> => {
      if (!req.userDetails) {
        throw StatusError.unauthorized('');
      }
      res.send(this.userService.genrateUserTokens(req.userDetails));
    },
  );
}
