import express, { Application, Request, Response, NextFunction } from 'express';
import bearerToken from 'express-bearer-token';
import { resolve } from 'path';
import bodyParser from 'body-parser';
import { connect as mongoConnect } from '@config/database/mongo';
import { v1Router } from './routes';
import * as i18n from 'i18n';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '@assets/swagger/swagger.json';
import { handleError } from '@config/handleErrors/handleError';
import { errors } from 'celebrate';
import { morganConf } from '@config/logger/logger';

class App {
  public app: Application;

  constructor() {
    this.app = express();
    this.initializeSwagger();
    this.initializeI18n();
    this.setHeaders();
    this.initializeMiddleware();
    this.initializeDBConnection();
    this.initializeRoutes();
    this.overrideExpressResponse();
  }

  /**
   * Initilization of internationalization(i18n)
   * default language english.
   */
  private initializeI18n(): void {
    i18n.configure({
      locales: ['en'],
      directory: resolve(__dirname, './assets/locales'),
    });
    this.app.use(i18n.init);
  }

  /**
   * Initilization of API's documentation.
   * We used Swagger 3.
   */
  private initializeSwagger(): void {
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

  /**
   * All express middleware goes here
   * `body-parser` = parsing request body
   * `bearerToken` = For `Basic Auth` token
   */
  private initializeMiddleware(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bearerToken());
    this.app.use(morganConf);
  }

  /**
   * Handaling database connection
   * In this application we are using only MongoDB with helper lib `mongoose`
   */
  private initializeDBConnection(): void {
    mongoConnect();
  }

  /**
   * Basic header configuartion
   * It is recomanded to update this section, depending on application's needs.
   * Security Attention: Take a special care of `Allow-Origin` for production
   * `Access-Control-Allow-Origin` - * or forward request origin not recomanded in production
   */
  private setHeaders(): void {
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      res.header('Access-Control-Allow-Origin', req.headers.origin);
      res.header('Access-Control-Allow-Headers', 'X-Requested-With');
      res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
      res.header('Access-Control-Allow-Credentials', 'true');
      res.header('Access-Control-Max-Age', '86400');
      res.header(
        'Access-Control-Allow-Headers',
        'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, accept-language, Authorization, x-api-key',
      );
      next();
    });
  }

  /**
   * Overriding the express response.
   * ok = 200
   * created = 201
   * noData = 204
   * badRequest = 400
   * forbidden = 403
   * severError = 500
   */
  private overrideExpressResponse(): void {
    this.app.use(errors());
    this.app.use(handleError);
  }

  /**
   * Initializing APIs base routes.
   * APIs base path also depends on server proxy configuration.
   */
  private initializeRoutes() {
    this.app.use('/api/v1', v1Router);
  }
}

/**
 * Export the application.
 * We made it singletone to avoid accidental double invokation.
 */
export default new App().app;
