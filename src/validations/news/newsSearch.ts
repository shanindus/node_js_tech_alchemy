import { celebrate, Joi } from 'celebrate';

export const newsSearch = celebrate({
  query: Joi.object({
    searchparam: Joi.string(),
  }),
});
