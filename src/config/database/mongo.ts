import { logger } from '@config/logger/logger';
import mongoose from 'mongoose';
import { envs } from '@config/env';
/**
 * This function is used for connecting database
 */
export const connect = (): void => {
  let connectioString = '';
  if (envs.db.username && envs.db.password) {
    connectioString = `mongodb://${envs.db.username}:${envs.db.password}@${envs.db.host}:${envs.db.port}/${envs.db.database}?authSource=${envs.db.authDatabase}`;
  } else {
    connectioString = `mongodb://${envs.db.host}:${envs.db.port}/${envs.db.database}`;
  }
  mongoose.connect(connectioString, envs.db.option, function (err) {
    if (err) {
      logger.error('Mongo DB Connection Error', err);
    } else {
      logger.info('Mongo DB Connected');
    }
  });
};

export const close = (): void => {
  mongoose.disconnect();
};
