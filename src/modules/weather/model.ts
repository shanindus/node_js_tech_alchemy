export interface IWeatherForecast {
  count: number;
  location: string;
  data: [IWeatherList];
}

export interface IWeatherList {
  date: string;
  main: string;
  temp: string;
}
