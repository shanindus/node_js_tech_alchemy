import { Document } from 'mongoose';
interface IUserName extends Document {
  first: string;
  last: string;
}

export interface IUser extends Document {
  name: IUserName;
  email: string;
  password: string;
}

export interface ICreateUserRequest {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface ILoginUserRequest {
  email: string;
  password: string;
}

export interface IUserTokens {
  name: IUserName;
  email: string;
  accessToken: string;
  accessTokenExpiry: number;
  refreshToken: string;
  refreshTokenExpiry: number;
}

export interface IUserRequestObject {
  _id: string;
  name: IUserName;
  email: string;
}
