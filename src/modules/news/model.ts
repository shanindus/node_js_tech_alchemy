export interface INewsHeadlines {
  count: number;
  data: [INewsArticles];
}

export interface INewsArticles {
  headline: string;
  link: string;
}
