import mongoose, { Schema } from 'mongoose';
import { IVoteCandidate } from './model';

const VoteCandidateSchema: Schema = new Schema(
  {
    name: {
      first: { type: String, required: true, trim: true },
      last: { type: String, require: true, trim: true },
    },
    email: {
      type: String,
      required: true,
    },
    image_url: {
      type: String,
      required: false,
    },
    title: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

// Export the model and return your IUser interface
export default mongoose.model<IVoteCandidate>(
  'voteCandidateModel',
  VoteCandidateSchema,
  'votecandidate',
);
