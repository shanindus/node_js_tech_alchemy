import { Document } from 'mongoose';

export interface IVoteCandidate extends Document {
  name: IUserName;
  email: string;
  image_url: string;
  title: string;
}

export interface IAddCandidateRequest {
  firstName: string;
  lastName: string;
  image_url: string;
  title: string;
  email: string;
}

interface IUserName extends Document {
  first: string;
  last: string;
}

export interface ICandidate extends Document {
  name: IUserName;
  email: string;
  image_url: string;
  title: string;
}
